////  SwiftUI2_ScrollViewReaderApp.swift
//  SwiftUI2_ScrollViewReader
//
//  Created on 07/02/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_ScrollViewReaderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
